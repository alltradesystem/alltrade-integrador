﻿namespace Alltrade.Integrador.Common.ServiceHost
{
    /// <summary>
    /// Service host.
    /// </summary>
    public interface IServiceHost
    {
        /// <summary>
        /// Run this instance.
        /// </summary>
        void Run();
    }
}
