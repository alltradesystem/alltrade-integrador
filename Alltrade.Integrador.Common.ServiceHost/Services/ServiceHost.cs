﻿using Alltrade.Integrador.Common.Commands;
using Alltrade.Integrador.Common.Events;
using Alltrade.Integrador.Common.Logger.Logger;
using Alltrade.Integrador.Common.RabbitMq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RawRabbit;
using Serilog;
using System;
using System.IO;

namespace Alltrade.Integrador.Common.ServiceHost.Services
{
    /// <summary>
    /// Service host.
    /// </summary>
    public class ServiceHost : IServiceHost
    {
        /// <summary>
        /// The web host.
        /// </summary>
        private readonly IWebHost _webHost;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Alltrade.Integrador.Common.Services.ServiceHost"/> class.
        /// </summary>
        /// <param name="webHost">Web host.</param>
        public ServiceHost(IWebHost webHost)
        {
            _webHost = webHost;
        }

        /// <summary>
        /// Run this instance.
        /// </summary>
        public void Run()
        {
            _webHost.Run();
        }

        /// <summary>
        /// Create the specified args.
        /// </summary>
        /// <returns>The create.</returns>
        /// <param name="args">Arguments.</param>
        /// <typeparam name="TStartup">The 1st type parameter.</typeparam>
        public static HostBuilder Create<TStartup>(string[] args)
            where TStartup : class
        {
            Console.Title = $"Microservice: {typeof(TStartup).Namespace} - Version: {typeof(TStartup).Assembly.GetName().Version}";

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();

            var webHostBuilder = WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging((context, logging) =>
                {
                    logging.ClearProviders();
                })
                .UseDefaultServiceProvider(options => options.ValidateScopes = false)
                .UseConfiguration(config)
                .UseLogger(config)
                .UseStartup<TStartup>();

            return new HostBuilder(webHostBuilder.Build());
        }

        /// <summary>
        /// Builder base.
        /// </summary>
        public abstract class BuilderBase
        {
            /// <summary>
            /// Build this instance.
            /// </summary>
            /// <returns>The build.</returns>
            public abstract ServiceHost Build();
        }

        /// <summary>
        /// Host builder.
        /// </summary>
        public class HostBuilder : BuilderBase
        {
            /// <summary>
            /// The web host.
            /// </summary>
            private readonly IWebHost _webHost;

            /// <summary>
            /// The bus.
            /// </summary>
            private IBusClient _bus;

            /// <summary>
            /// Initializes a new instance of the
            /// <see cref="T:Alltrade.Integrador.Common.Services.ServiceHost.HostBuilder"/> class.
            /// </summary>
            /// <param name="webHost">Web host.</param>
            public HostBuilder(IWebHost webHost)
            {
                _webHost = webHost;
            }

            /// <summary>
            /// Uses the rabbit mq.
            /// </summary>
            /// <returns>The rabbit mq.</returns>
            public BusBuilder UseRabbitMq()
            {
                _bus = (IBusClient)_webHost.Services.GetService(typeof(IBusClient));
                return new BusBuilder(_webHost, _bus);
            }

            /// <inheritdoc />
            /// <summary>
            /// Build this instance.
            /// </summary>
            /// <returns>The build.</returns>
            public override ServiceHost Build()
            {
                return new ServiceHost(_webHost);
            }
        }

        /// <summary>
        /// Bus builder.
        /// </summary>
        public class BusBuilder : BuilderBase
        {
            /// <summary>
            /// The web host.
            /// </summary>
            private readonly IWebHost _webHost;

            /// <summary>
            /// The bus.
            /// </summary>
            private IBusClient _bus;

            /// <summary>
            /// Initializes a new instance of the
            /// <see cref="T:Alltrade.Integrador.Common.Services.ServiceHost.BusBuilder"/> class.
            /// </summary>
            /// <param name="webHost">Web host.</param>
            /// <param name="bus">Bus.</param>
            public BusBuilder(IWebHost webHost, IBusClient bus)
            {
                _webHost = webHost;
                _bus = bus;
            }

            /// <summary>
            /// Subscribes to command.
            /// </summary>
            /// <returns>The to command.</returns>
            /// <typeparam name="TCommand">The 1st type parameter.</typeparam>
            public BusBuilder SubscribeToCommand<TCommand>() where TCommand : ICommand
            {
                var handler = (ICommandHandler<TCommand>)_webHost.Services
                    .GetService(typeof(ICommandHandler<TCommand>));

                _bus.WithCommandHandlerAsync(handler);
                return this;
            }

            /// <summary>
            /// Subscribes to event.
            /// </summary>
            /// <returns>The to event.</returns>
            /// <typeparam name="TEvent">The 1st type parameter.</typeparam>
            public BusBuilder SubscribeToEvent<TEvent>() where TEvent : IEvent
            {
                var handler = (IEventHandler<TEvent>)_webHost.Services
                    .GetService(typeof(IEventHandler<TEvent>));

                _bus.WithEventHandlerAsync(handler);
                return this;
            }

            /// <inheritdoc />
            /// <summary>
            /// Build this instance.
            /// </summary>
            /// <returns>The build.</returns>
            public override ServiceHost Build()
            {
                return new ServiceHost(_webHost);
            }
        }
    }
}
