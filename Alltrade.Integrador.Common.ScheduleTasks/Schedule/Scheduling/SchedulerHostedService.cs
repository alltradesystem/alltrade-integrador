using Alltrade.Integrador.Common.Exceptions.Exceptions.Utils.Assert;
using Alltrade.Integrador.Common.Logger.Logger;
using Alltrade.Integrador.Common.Schedule.Schedule.CronTab;
using Alltrade.Integrador.Common.Schedule.Schedule.Scheduling.Attributes;
using Alltrade.Integrador.Common.Utils.Assert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Alltrade.Integrador.Common.Schedule.Schedule.Scheduling
{
    /// <summary>
    /// Scheduler hosted service.
    /// </summary>
    public class SchedulerHostedService : HostedService
    {
        /// <summary>
        /// The base logger.
        /// </summary>
        private readonly IBaseLogger _logger;

        /// <summary>
        /// Occurs when unobserved task exception.
        /// </summary>
        public event EventHandler<UnobservedTaskExceptionEventArgs> UnobservedTaskException;

        /// <summary>
        /// The scheduled tasks.
        /// </summary>
        private readonly List<SchedulerTaskWrapper> _scheduledTasks = new List<SchedulerTaskWrapper>();

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Schedule.Schedule.Scheduling.SchedulerHostedService"/> class.
        /// </summary>
        /// <param name="baseLogger"></param>
        /// <param name="scheduledTasks">Scheduled tasks.</param>
        public SchedulerHostedService(IBaseLogger baseLogger, IEnumerable<IScheduledTask> scheduledTasks)
        {
            _logger = baseLogger;

            var referenceTime = DateTime.UtcNow;
            foreach (var scheduledTask in scheduledTasks)
            {
                try
                {
                    var scheduleAttribute = (ScheduleTask)Attribute.GetCustomAttribute(scheduledTask.GetType(),
                        typeof(ScheduleTask));

                    var scheduleClassName = scheduledTask.GetType().Name;

                    AssertionConcern.AssertArgumentNotNull(scheduleAttribute, $"'Schedule Task' attribute can not be found in the class: '{scheduleClassName}'");
                    AssertionConcern.AssertArgumentNotNull(scheduleAttribute.Name, $"'Schedule Task' attribute property 'Name' can not be null in the class: '{scheduleClassName}'");
                    AssertionConcern.AssertArgumentNotEmpty(scheduleAttribute.Name, $"'Schedule Task' attribute property 'Name' can not be empty in the class: '{scheduleClassName}'");
                    AssertionConcern.AssertArgumentNotNull(scheduleAttribute.Schedule, $"'Schedule Task' attribute property 'Schedule' can not be null in the class: '{scheduleClassName}'");
                    AssertionConcern.AssertArgumentNotEmpty(scheduleAttribute.Schedule, $"'Schedule Task' attribute property 'Schedule' can not be empty in the class: '{scheduleClassName}'");

                    var scheduleTaskRegistered = _scheduledTasks.FirstOrDefault(sc => sc.ScheduleTaskAttribute != null && sc.ScheduleTaskAttribute.Name == scheduleAttribute.Name);
                    if (scheduleTaskRegistered != null)
                    {
                        throw new AssertionConcernException($": The Schedule Task '{scheduleAttribute.Name}' already been registered in class: '${scheduleTaskRegistered.Task.GetType().Name}'");
                    }

                    _scheduledTasks.Add(new SchedulerTaskWrapper
                    {
                        Schedule = CronTabSchedule.Parse(scheduleAttribute.Schedule),
                        ScheduleTaskAttribute = scheduleAttribute,
                        Task = scheduledTask,
                        NextRunTime = referenceTime
                    });
                }
                catch (AssertionConcernException ex)
                {
                    _logger.Log(ex, "Schedule Task cannot be registered. Due to the rule: ", LogEvent.Fatal);
                }
                catch (Exception ex)
                {
                    _logger.Log(ex, LogEvent.Fatal);
                }
            }
        }

        /// <summary>
        /// Executes the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="cancellationToken">Cancellation token.</param>
        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await ExecuteOnceAsync(cancellationToken);
                await Task.Delay(TimeSpan.FromMinutes(1), cancellationToken);
            }
        }

        /// <summary>
        /// Executes the once async.
        /// </summary>
        /// <returns>The once async.</returns>
        /// <param name="cancellationToken">Cancellation token.</param>
        private async Task ExecuteOnceAsync(CancellationToken cancellationToken)
        {
            var referenceTime = DateTime.UtcNow;
            var taskFactory = new TaskFactory(TaskScheduler.Current);
            var tasksThatShouldRun = _scheduledTasks.Where(t => t.ShouldRun(referenceTime)).ToList();

            foreach (var taskThatShouldRun in tasksThatShouldRun)
            {
                taskThatShouldRun.Increment();

                await taskFactory.StartNew(async () =>
                {
                    var isTaskRunnerWithSuccess = false;
                    var attr = taskThatShouldRun.ScheduleTaskAttribute;

                    try
                    {
                        _logger.Log($"[RUNNING] Running Task: '{attr.Name}' - Schedule: '{attr.Schedule}'.", LogEvent.Information);
                        await taskThatShouldRun.Task.ExecuteAsync(cancellationToken);

                        isTaskRunnerWithSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        var args = new UnobservedTaskExceptionEventArgs(
                            ex as AggregateException ?? new AggregateException(ex));

                        UnobservedTaskException?.Invoke(this, args);
                        if (!args.Observed)
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        if (isTaskRunnerWithSuccess)
                        {
                            _logger.Log($"[SUCCESS] Runner Task: '{attr.Name}' - Schedule: '{attr.Schedule}'.", LogEvent.Information);
                        }
                        else
                        {
                            _logger.Log($"[FAILED] Runner Task: '{attr.Name}' - Schedule: '{attr.Schedule}'.", LogEvent.Information);
                        }
                    }
                }, cancellationToken);
            }
        }

        /// <summary>
        /// Scheduler task wrapper.
        /// </summary>
        private class SchedulerTaskWrapper
        {
            /// <summary>
            /// Gets or sets the schedule.
            /// </summary>
            /// <value>The schedule.</value>
            public CronTabSchedule Schedule { get; set; }

            public ScheduleTask ScheduleTaskAttribute { get; set; }

            /// <summary>
            /// Gets or sets the task.
            /// </summary>
            /// <value>The task.</value>
            public IScheduledTask Task { get; set; }

            /// <summary>
            /// Gets or sets the last run time.
            /// </summary>
            /// <value>The last run time.</value>
            public DateTime LastRunTime { get; set; }

            /// <summary>
            /// Gets or sets the next run time.
            /// </summary>
            /// <value>The next run time.</value>
            public DateTime NextRunTime { get; set; }

            /// <summary>
            /// Increment this instance.
            /// </summary>
            public void Increment()
            {
                LastRunTime = NextRunTime;
                NextRunTime = Schedule.GetNextOccurrence(NextRunTime);
            }

            /// <summary>
            /// Shoulds the run.
            /// </summary>
            /// <returns><c>true</c>, if run was shoulded, <c>false</c> otherwise.</returns>
            /// <param name="currentTime">Current time.</param>
            public bool ShouldRun(DateTime currentTime)
            {
                return NextRunTime < currentTime && LastRunTime != NextRunTime;
            }
        }
    }
}