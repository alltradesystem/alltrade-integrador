using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;

namespace Alltrade.Integrador.Common.Schedule.Schedule.Scheduling
{
    /// <summary>
    /// Hosted service.
    /// </summary>
    public abstract class HostedService : IHostedService
    {
        /// <summary>
        /// Gets or sets the executing task.
        /// </summary>
        /// <value>The executing task.</value>
        private Task _executingTask { get; set; }

        /// <summary>
        /// Gets or sets the cts.
        /// </summary>
        /// <value>The cts.</value>
        private CancellationTokenSource _cts { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Starts the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="cancellationToken">Cancellation token.</param>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            // Create a linked token so we can trigger cancellation outside of this token's cancellation
            _cts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);

            // Store the task we're executing
            _executingTask = ExecuteAsync(_cts.Token);

            // If the task is completed then return it, otherwise it's running
            return _executingTask.IsCompleted ? _executingTask : Task.CompletedTask;
        }

        /// <inheritdoc />
        /// <summary>
        /// Stops the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            // Stop called without start
            if (_executingTask == null)
            {
                return;
            }

            // Signal cancellation to the executing method
            _cts.Cancel();

            // Wait until the task completes or the stop token triggers
            await Task.WhenAny(_executingTask, Task.Delay(-1, cancellationToken));

            // Throw if cancellation triggered
            cancellationToken.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Derived classes should override this and execute a long running method until
        /// cancellation is requested
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="cancellationToken">Cancellation token.</param>
        protected abstract Task ExecuteAsync(CancellationToken cancellationToken);
    }
}