using System.Threading;
using System.Threading.Tasks;

namespace Alltrade.Integrador.Common.Schedule.Schedule.Scheduling
{
    /// <summary>
    /// Scheduled task.
    /// </summary>
    public interface IScheduledTask
    {
        /// <summary>
        /// Executes the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="cancellationToken">Cancellation token.</param>
        Task ExecuteAsync(CancellationToken cancellationToken);
    }
}