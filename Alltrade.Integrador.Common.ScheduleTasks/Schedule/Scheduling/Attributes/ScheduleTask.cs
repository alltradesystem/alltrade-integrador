﻿using System;
namespace Alltrade.Integrador.Common.Schedule.Schedule.Scheduling.Attributes
{

    /// <inheritdoc />
    /// <summary>
    /// Schedule task.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
	public class ScheduleTask : Attribute
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the schedule.
        /// </summary>
        /// <value>The schedule.</value>
        public string Schedule { get; set; }
    }
}
