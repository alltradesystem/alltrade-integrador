using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;

namespace Alltrade.Integrador.Common.Schedule.Schedule.CronTab
{
    /// <summary>
    /// Cron tab field impl.
    /// </summary>
    [Serializable]
    public sealed class CronTabFieldImpl : IObjectReference
    {
        /// <summary>
        /// The minute.
        /// </summary>
        public static readonly CronTabFieldImpl Minute = new CronTabFieldImpl(CronTabFieldKind.Minute, 0, 59, null);

        /// <summary>
        /// The hour.
        /// </summary>
        public static readonly CronTabFieldImpl Hour = new CronTabFieldImpl(CronTabFieldKind.Hour, 0, 23, null);

        /// <summary>
        /// The day.
        /// </summary>
        public static readonly CronTabFieldImpl Day = new CronTabFieldImpl(CronTabFieldKind.Day, 1, 31, null);

        /// <summary>
        /// The month.
        /// </summary>
        public static readonly CronTabFieldImpl Month = new CronTabFieldImpl(CronTabFieldKind.Month, 1, 12,
            new[]
            {
                "January", "February", "March", "April",
                "May", "June", "July", "August",
                "September", "October", "November",
                "December"
            });

        /// <summary>
        /// The day of week.
        /// </summary>
        public static readonly CronTabFieldImpl DayOfWeek = new CronTabFieldImpl(CronTabFieldKind.DayOfWeek, 0, 6,
            new[]
            {
                "Sunday", "Monday", "Tuesday",
                "Wednesday", "Thursday", "Friday",
                "Saturday"
            });

        /// <summary>
        /// The kind of the field by.
        /// </summary>
        private static readonly CronTabFieldImpl[] FieldByKind = { Minute, Hour, Day, Month, DayOfWeek };

        /// <summary>
        /// The comparer.
        /// </summary>
        private static readonly CompareInfo Comparer = CultureInfo.InvariantCulture.CompareInfo;

        /// <summary>
        /// The comma.
        /// </summary>
        private static readonly char[] Comma = { ',' };

        /// <summary>
        /// The kind.
        /// </summary>
        private readonly CronTabFieldKind _kind;

        /// <summary>
        /// The max value.
        /// </summary>
        private readonly int _maxValue;

        /// <summary>
        /// The minimum value.
        /// </summary>
        private readonly int _minValue;

        /// <summary>
        /// The names.
        /// </summary>
        private readonly string[] _names;


        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Schedule.Schedule.CronTab.CronTabFieldImpl"/> class.
        /// </summary>
        /// <param name="kind">Kind.</param>
        /// <param name="minValue">Minimum value.</param>
        /// <param name="maxValue">Max value.</param>
        /// <param name="names">Names.</param>
        private CronTabFieldImpl(CronTabFieldKind kind, int minValue, int maxValue, string[] names)
        {
            Debug.Assert(Enum.IsDefined(typeof(CronTabFieldKind), kind));
            Debug.Assert(minValue >= 0);
            Debug.Assert(maxValue >= minValue);
            Debug.Assert(names == null || names.Length == (maxValue - minValue + 1));

            _kind = kind;
            _minValue = minValue;
            _maxValue = maxValue;
            _names = names;
        }

        /// <summary>
        /// Gets the kind.
        /// </summary>
        /// <value>The kind.</value>
        public CronTabFieldKind Kind
        {
            get { return _kind; }
        }

        /// <summary>
        /// Gets the minimum value.
        /// </summary>
        /// <value>The minimum value.</value>
        public int MinValue
        {
            get { return _minValue; }
        }

        /// <summary>
        /// Gets the max value.
        /// </summary>
        /// <value>The max value.</value>
        public int MaxValue
        {
            get { return _maxValue; }
        }

        /// <summary>
        /// Gets the value count.
        /// </summary>
        /// <value>The value count.</value>
        public int ValueCount
        {
            get { return _maxValue - _minValue + 1; }
        }

        #region IObjectReference Members

        /// <summary>
        /// System.s the runtime. serialization. IO bject reference. get real object.
        /// </summary>
        /// <returns>The runtime. serialization. IO bject reference. get real object.</returns>
        /// <param name="context">Context.</param>
        object IObjectReference.GetRealObject(StreamingContext context)
        {
            return FromKind(Kind);
        }

        #endregion

        /// <summary>
        /// Froms the kind.
        /// </summary>
        /// <returns>The kind.</returns>
        /// <param name="kind">Kind.</param>
        public static CronTabFieldImpl FromKind(CronTabFieldKind kind)
        {
            if (!Enum.IsDefined(typeof(CronTabFieldKind), kind))
            {
                throw new ArgumentException(string.Format(
                    "Invalid crontab field kind. Valid values are {0}.",
                    string.Join(", ", Enum.GetNames(typeof(CronTabFieldKind)))), nameof(kind));
            }

            return FieldByKind[(int)kind];
        }

        /// <summary>
        /// Format the specified field, writer and noNames.
        /// </summary>
        /// <param name="field">Field.</param>
        /// <param name="writer">Writer.</param>
        /// <param name="noNames">If set to <c>true</c> no names.</param>
        public void Format(CronTabField field, TextWriter writer, bool noNames)
        {
            if (field == null)
                throw new ArgumentNullException(nameof(field));

            if (writer == null)
                throw new ArgumentNullException(nameof(writer));

            var next = field.GetFirst();
            var count = 0;

            while (next != -1)
            {
                var first = next;
                int last;

                do
                {
                    last = next;
                    next = field.Next(last + 1);
                } while (next - last == 1);

                if (count == 0
                    && first == _minValue && last == _maxValue)
                {
                    writer.Write('*');
                    return;
                }

                if (count > 0)
                    writer.Write(',');

                if (first == last)
                {
                    FormatValue(first, writer, noNames);
                }
                else
                {
                    FormatValue(first, writer, noNames);
                    writer.Write('-');
                    FormatValue(last, writer, noNames);
                }

                count++;
            }
        }

        /// <summary>
        /// Formats the value.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="writer">Writer.</param>
        /// <param name="noNames">If set to <c>true</c> no names.</param>
        private void FormatValue(int value, TextWriter writer, bool noNames)
        {
            Debug.Assert(writer != null);

            if (noNames || _names == null)
            {
                if (value >= 0 && value < 100)
                {
                    FastFormatNumericValue(value, writer);
                }
                else
                {
                    writer.Write(value.ToString(CultureInfo.InvariantCulture));
                }
            }
            else
            {
                var index = value - _minValue;
                writer.Write((string) _names[index]);
            }
        }

        /// <summary>
        /// Fasts the format numeric value.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="writer">Writer.</param>
        private static void FastFormatNumericValue(int value, TextWriter writer)
        {
            Debug.Assert(value >= 0 && value < 100);
            Debug.Assert(writer != null);

            if (value >= 10)
            {
                writer.Write((char)('0' + (value / 10)));
                writer.Write((char)('0' + (value % 10)));
            }
            else
            {
                writer.Write((char)('0' + value));
            }
        }

        /// <summary>
        /// Parse the specified str and acc.
        /// </summary>
        /// <param name="str">String.</param>
        /// <param name="acc">Acc.</param>
        public void Parse(string str, CronTabFieldAccumulator acc)
        {
            if (acc == null)
                throw new ArgumentNullException(nameof(acc));

            if (string.IsNullOrEmpty(str))
                return;

            try
            {
                InternalParse(str, acc);
            }
            catch (FormatException e)
            {
                ThrowParseException(e, str);
            }
        }

        /// <summary>
        /// Throws the parse exception.
        /// </summary>
        /// <param name="innerException">Inner exception.</param>
        /// <param name="str">String.</param>
        private static void ThrowParseException(Exception innerException, string str)
        {
            Debug.Assert(str != null);
            Debug.Assert(innerException != null);

            throw new FormatException(string.Format("'{0}' is not a valid crontab field expression.", str),
                innerException);
        }

        /// <summary>
        /// Internals the parse.
        /// </summary>
        /// <param name="str">String.</param>
        /// <param name="acc">Acc.</param>
        private void InternalParse(string str, CronTabFieldAccumulator acc)
        {
            Debug.Assert(str != null);
            Debug.Assert(acc != null);

            if (str.Length == 0)
                throw new FormatException("A crontab field value cannot be empty.");

            //
            // Next, look for a list of values (e.g. 1,2,3).
            //

            var commaIndex = str.IndexOf(",", StringComparison.Ordinal);

            if (commaIndex > 0)
            {
                foreach (var token in str.Split(Comma))
                    InternalParse(token, acc);
            }
            else
            {
                var every = 1;

                //
                // Look for stepping first (e.g. */2 = every 2nd).
                // 

                var slashIndex = str.IndexOf("/", StringComparison.Ordinal);

                if (slashIndex > 0)
                {
                    every = int.Parse(str.Substring(slashIndex + 1), CultureInfo.InvariantCulture);
                    str = str.Substring(0, slashIndex);
                }

                //
                // Next, look for wildcard (*).
                //

                if (str.Length == 1 && str[0] == '*')
                {
                    acc(-1, -1, every);
                    return;
                }

                //
                // Next, look for a range of values (e.g. 2-10).
                //

                var dashIndex = str.IndexOf("-", StringComparison.Ordinal);

                if (dashIndex > 0)
                {
                    var first = ParseValue(str.Substring(0, dashIndex));
                    var last = ParseValue(str.Substring(dashIndex + 1));

                    acc(first, last, every);
                    return;
                }

                //
                // Finally, handle the case where there is only one number.
                //

                var value = ParseValue(str);

                if (every == 1)
                {
                    acc(value, value, 1);
                }
                else
                {
                    Debug.Assert(every != 0);

                    acc(value, _maxValue, every);
                }
            }
        }

        /// <summary>
        /// Parses the value.
        /// </summary>
        /// <returns>The value.</returns>
        /// <param name="str">String.</param>
        private int ParseValue(string str)
        {
            Debug.Assert(str != null);

            if (str.Length == 0)
                throw new FormatException("A crontab field value cannot be empty.");

            var firstChar = str[0];

            if (firstChar >= '0' && firstChar <= '9')
                return int.Parse(str, CultureInfo.InvariantCulture);

            if (_names == null)
            {
                throw new FormatException(string.Format(
                    "'{0}' is not a valid value for this crontab field. It must be a numeric value between {1} and {2} (all inclusive).",
                    str, _minValue, _maxValue));
            }

            for (var i = 0; i < _names.Length; i++)
            {
                if (Comparer.IsPrefix(_names[i], str, CompareOptions.IgnoreCase))
                    return i + _minValue;
            }

            throw new FormatException(string.Format(
                "'{0}' is not a known value name. Use one of the following: {1}.",
                str, string.Join(", ", _names)));
        }
    }
}