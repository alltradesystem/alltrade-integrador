namespace Alltrade.Integrador.Common.Schedule.Schedule.CronTab
{
    /// <summary>
    /// Cron tab field accumulator.
    /// </summary>
    public delegate void CronTabFieldAccumulator(int start, int end, int interval);
}