using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;

namespace Alltrade.Integrador.Common.Schedule.Schedule.CronTab
{
    /// <summary>
    /// Cron tab schedule.
    /// </summary>
    [Serializable]
    public sealed class CronTabSchedule
    {
        /// <summary>
        /// The separators.
        /// </summary>
        private static readonly char[] Separators = { ' ' };

        /// <summary>
        /// The days.
        /// </summary>
        private readonly CronTabField _days;

        /// <summary>
        /// The days of week.
        /// </summary>
        private readonly CronTabField _daysOfWeek;

        /// <summary>
        /// The hours.
        /// </summary>
        private readonly CronTabField _hours;

        /// <summary>
        /// The minutes.
        /// </summary>
        private readonly CronTabField _minutes;

        /// <summary>
        /// The months.
        /// </summary>
        private readonly CronTabField _months;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Schedule.Schedule.CronTab.CronTabSchedule"/> class.
        /// </summary>
        /// <param name="expression">Expression.</param>
        private CronTabSchedule(string expression)
        {
            Debug.Assert(expression != null);

            var fields = expression.Split((char[]) Separators, StringSplitOptions.RemoveEmptyEntries);

            if (fields.Length != 5)
            {
                throw new FormatException(string.Format(
                    "'{0}' is not a valid crontab expression. It must contain at least 5 components of a schedule "
                    + "(in the sequence of minutes, hours, days, months, days of week).",
                    expression));
            }

            _minutes = CronTabField.Minutes(fields[0]);
            _hours = CronTabField.Hours(fields[1]);
            _days = CronTabField.Days(fields[2]);
            _months = CronTabField.Months(fields[3]);
            _daysOfWeek = CronTabField.DaysOfWeek(fields[4]);
        }

        /// <summary>
        /// Gets the calendar.
        /// </summary>
        /// <value>The calendar.</value>
        private static Calendar Calendar
        {
            get { return CultureInfo.InvariantCulture.Calendar; }
        }

        /// <summary>
        /// Parse the specified expression.
        /// </summary>
        /// <returns>The parse.</returns>
        /// <param name="expression">Expression.</param>
        public static CronTabSchedule Parse(string expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            return new CronTabSchedule(expression);
        }

        /// <summary>
        /// Gets the next occurrences.
        /// </summary>
        /// <returns>The next occurrences.</returns>
        /// <param name="baseTime">Base time.</param>
        /// <param name="endTime">End time.</param>
        public IEnumerable<DateTime> GetNextOccurrences(DateTime baseTime, DateTime endTime)
        {
            for (var occurrence = GetNextOccurrence(baseTime, endTime);
                occurrence < endTime;
                occurrence = GetNextOccurrence(occurrence, endTime))
            {
                yield return occurrence;
            }
        }

        /// <summary>
        /// Gets the next occurrence.
        /// </summary>
        /// <returns>The next occurrence.</returns>
        /// <param name="baseTime">Base time.</param>
        public DateTime GetNextOccurrence(DateTime baseTime)
        {
            return GetNextOccurrence(baseTime, DateTime.MaxValue);
        }

        /// <summary>
        /// Gets the next occurrence.
        /// </summary>
        /// <returns>The next occurrence.</returns>
        /// <param name="baseTime">Base time.</param>
        /// <param name="endTime">End time.</param>
        public DateTime GetNextOccurrence(DateTime baseTime, DateTime endTime)
        {
            const int nil = -1;

            var baseYear = baseTime.Year;
            var baseMonth = baseTime.Month;
            var baseDay = baseTime.Day;
            var baseHour = baseTime.Hour;
            var baseMinute = baseTime.Minute;

            var endYear = endTime.Year;
            var endMonth = endTime.Month;
            var endDay = endTime.Day;

            var year = baseYear;
            var month = baseMonth;
            var day = baseDay;
            var hour = baseHour;
            var minute = baseMinute + 1;

            //
            // Minute
            //

            minute = _minutes.Next(minute);

            if (minute == nil)
            {
                minute = _minutes.GetFirst();
                hour++;
            }

            //
            // Hour
            //

            hour = _hours.Next(hour);

            if (hour == nil)
            {
                minute = _minutes.GetFirst();
                hour = _hours.GetFirst();
                day++;
            }
            else if (hour > baseHour)
            {
                minute = _minutes.GetFirst();
            }

            //
            // Day
            //

            day = _days.Next(day);

            RetryDayMonth:

            if (day == nil)
            {
                minute = _minutes.GetFirst();
                hour = _hours.GetFirst();
                day = _days.GetFirst();
                month++;
            }
            else if (day > baseDay)
            {
                minute = _minutes.GetFirst();
                hour = _hours.GetFirst();
            }

            //
            // Month
            //

            month = _months.Next(month);

            if (month == nil)
            {
                minute = _minutes.GetFirst();
                hour = _hours.GetFirst();
                day = _days.GetFirst();
                month = _months.GetFirst();
                year++;
            }
            else if (month > baseMonth)
            {
                minute = _minutes.GetFirst();
                hour = _hours.GetFirst();
                day = _days.GetFirst();
            }

            //
            // The day field in a cron expression spans the entire range of days
            // in a month, which is from 1 to 31. However, the number of days in
            // a month tend to be variable depending on the month (and the year
            // in case of February). So a check is needed here to see if the
            // date is a border case. If the day happens to be beyond 28
            // (meaning that we're dealing with the suspicious range of 29-31)
            // and the date part has changed then we need to determine whether
            // the day still makes sense for the given year and month. If the
            // day is beyond the last possible value, then the day/month part
            // for the schedule is re-evaluated. So an expression like "0 0
            // 15,31 * *" will yield the following sequence starting on midnight
            // of Jan 1, 2000:
            //
            //  Jan 15, Jan 31, Feb 15, Mar 15, Apr 15, Apr 31, ...
            //

            var dateChanged = day != baseDay || month != baseMonth || year != baseYear;

            if (day > 28 && dateChanged && day > Calendar.GetDaysInMonth(year, month))
            {
                if (year >= endYear && month >= endMonth && day >= endDay)
                    return endTime;

                day = nil;
                goto RetryDayMonth;
            }

            var nextTime = new DateTime(year, month, day, hour, minute, 0, 0, baseTime.Kind);

            if (nextTime >= endTime)
                return endTime;

            //
            // Day of week
            //

            if (_daysOfWeek.Contains((int)nextTime.DayOfWeek))
                return nextTime;

            return GetNextOccurrence(new DateTime(year, month, day, 23, 59, 0, 0, baseTime.Kind), endTime);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Alltrade.Integrador.Common.Schedule.Schedule.CronTab.CronTabSchedule"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Alltrade.Integrador.Common.Schedule.Schedule.CronTab.CronTabSchedule"/>.</returns>
        public override string ToString()
        {
            var writer = new StringWriter(CultureInfo.InvariantCulture);

            _minutes.Format(writer, true);
            writer.Write(' ');
            _hours.Format(writer, true);
            writer.Write(' ');
            _days.Format(writer, true);
            writer.Write(' ');
            _months.Format(writer, true);
            writer.Write(' ');
            _daysOfWeek.Format(writer, true);

            return writer.ToString();
        }
    }
}