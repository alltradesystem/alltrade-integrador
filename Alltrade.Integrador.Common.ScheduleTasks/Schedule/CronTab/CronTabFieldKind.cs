using System;

namespace Alltrade.Integrador.Common.Schedule.Schedule.CronTab
{
    /// <summary>
    /// Cron tab field kind.
    /// </summary>
    [Serializable]
    public enum CronTabFieldKind
    {
        Minute,
        Hour,
        Day,
        Month,
        DayOfWeek
    }
}