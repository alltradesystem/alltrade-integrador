using System;
using System.Collections;
using System.Globalization;
using System.IO;

namespace Alltrade.Integrador.Common.Schedule.Schedule.CronTab
{
    /// <summary>
    /// Cron tab field.
    /// </summary>
    [Serializable]
    public sealed class CronTabField
    {
        /// <summary>
        /// The bits.
        /// </summary>
        private readonly BitArray _bits;

        /// <summary>
        /// The impl.
        /// </summary>
        private readonly CronTabFieldImpl _impl;

        /// <summary>
        /// The max value set.
        /// </summary>
        private int _maxValueSet;

        /// <summary>
        /// The minimum value set.
        /// </summary>
        private int _minValueSet;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Schedule.Schedule.CronTab.CronTabField"/> class.
        /// </summary>
        /// <param name="impl">Impl.</param>
        /// <param name="expression">Expression.</param>
        private CronTabField(CronTabFieldImpl impl, string expression)
        {
            _impl = impl ?? throw new ArgumentNullException(nameof(impl));
            _bits = new BitArray(impl.ValueCount);

            _bits.SetAll(false);
            _minValueSet = int.MaxValue;
            _maxValueSet = -1;

            _impl.Parse(expression, Accumulate);
        }

        #region ICrontabField Members

        /// <summary>
        /// Gets the first value of the field or -1.
        /// </summary>
        /// <returns>The first.</returns>
        public int GetFirst()
        {
            return _minValueSet < int.MaxValue ? _minValueSet : -1;
        }

        /// <summary>
        /// Gets the next value of the field that occurs after the given 
        /// start value or -1 if there is no next value available.
        /// </summary>
        /// <returns>The next.</returns>
        /// <param name="start">Start.</param>
        public int Next(int start)
        {
            if (start < _minValueSet)
                return _minValueSet;

            var startIndex = ValueToIndex(start);
            var lastIndex = ValueToIndex(_maxValueSet);

            for (var i = startIndex; i <= lastIndex; i++)
            {
                if (_bits[i])
                    return IndexToValue(i);
            }

            return -1;
        }

        /// <summary>
        /// Determines if the given value occurs in the field.
        /// </summary>
        /// <returns>The contains.</returns>
        /// <param name="value">Value.</param>
        public bool Contains(int value)
        {
            return _bits[ValueToIndex(value)];
        }

        #endregion

        /// <summary>
        /// Parses a crontab field expression given its kind.
        /// </summary>
        /// <returns>The parse.</returns>
        /// <param name="kind">Kind.</param>
        /// <param name="expression">Expression.</param>
        public static CronTabField Parse(CronTabFieldKind kind, string expression)
        {
            return new CronTabField(CronTabFieldImpl.FromKind(kind), expression);
        }

        /// <summary>
        /// Parses a crontab field expression representing minutes.
        /// </summary>
        /// <returns>The minutes.</returns>
        /// <param name="expression">Expression.</param>
        public static CronTabField Minutes(string expression)
        {
            return new CronTabField(CronTabFieldImpl.Minute, expression);
        }

        /// <summary>
        /// Parses a crontab field expression representing hours.
        /// </summary>
        /// <returns>The hours.</returns>
        /// <param name="expression">Expression.</param>
        public static CronTabField Hours(string expression)
        {
            return new CronTabField(CronTabFieldImpl.Hour, expression);
        }

        /// <summary>
        /// Parses a crontab field expression representing days in any given month.
        /// </summary>
        /// <returns>The days.</returns>
        /// <param name="expression">Expression.</param>
        public static CronTabField Days(string expression)
        {
            return new CronTabField(CronTabFieldImpl.Day, expression);
        }

        /// <summary>
        /// Parses a crontab field expression representing months.
        /// </summary>
        /// <returns>The months.</returns>
        /// <param name="expression">Expression.</param>
        public static CronTabField Months(string expression)
        {
            return new CronTabField(CronTabFieldImpl.Month, expression);
        }

        /// <summary>
        /// Parses a crontab field expression representing days of a week.
        /// </summary>
        /// <returns>The of week.</returns>
        /// <param name="expression">Expression.</param>
        public static CronTabField DaysOfWeek(string expression)
        {
            return new CronTabField(CronTabFieldImpl.DayOfWeek, expression);
        }

        /// <summary>
        /// Indexs to value.
        /// </summary>
        /// <returns>The to value.</returns>
        /// <param name="index">Index.</param>
        private int IndexToValue(int index)
        {
            return index + _impl.MinValue;
        }

        /// <summary>
        /// Values the index of the to.
        /// </summary>
        /// <returns>The to index.</returns>
        /// <param name="value">Value.</param>
        private int ValueToIndex(int value)
        {
            return value - _impl.MinValue;
        }

        /// <summary>
        /// Accumulates the given range (start to end) and interval of values
        /// into the current set of the field.
        /// </summary>
        /// <remarks>
        /// To set the entire range of values representable by the field,
        /// set <param name="start" /> and <param name="end" /> to -1 and
        /// <param name="interval" /> to 1.
        /// </remarks>
        private void Accumulate(int start, int end, int interval)
        {
            var minValue = _impl.MinValue;
            var maxValue = _impl.MaxValue;

            if (start == end)
            {
                if (start < 0)
                {
                    //
                    // We're setting the entire range of values.
                    //

                    if (interval <= 1)
                    {
                        _minValueSet = minValue;
                        _maxValueSet = maxValue;
                        _bits.SetAll(true);
                        return;
                    }

                    start = minValue;
                    end = maxValue;
                }
                else
                {
                    //
                    // We're only setting a single value - check that it is in range.
                    //

                    if (start < minValue)
                    {
                        throw new FormatException(string.Format(
                            "'{0} is lower than the minimum allowable value for this field. Value must be between {1} and {2} (all inclusive).",
                            start, _impl.MinValue, _impl.MaxValue));
                    }

                    if (start > maxValue)
                    {
                        throw new FormatException(string.Format(
                            "'{0} is higher than the maximum allowable value for this field. Value must be between {1} and {2} (all inclusive).",
                            end, _impl.MinValue, _impl.MaxValue));
                    }
                }
            }
            else
            {
                //
                // For ranges, if the start is bigger than the end value then
                // swap them over.
                //

                if (start > end)
                {
                    end ^= start;
                    start ^= end;
                    end ^= start;
                }

                if (start < 0)
                {
                    start = minValue;
                }
                else if (start < minValue)
                {
                    throw new FormatException(string.Format(
                        "'{0} is lower than the minimum allowable value for this field. Value must be between {1} and {2} (all inclusive).",
                        start, _impl.MinValue, _impl.MaxValue));
                }

                if (end < 0)
                {
                    end = maxValue;
                }
                else if (end > maxValue)
                {
                    throw new FormatException(string.Format(
                        "'{0} is higher than the maximum allowable value for this field. Value must be between {1} and {2} (all inclusive).",
                        end, _impl.MinValue, _impl.MaxValue));
                }
            }

            if (interval < 1)
                interval = 1;

            int i;

            //
            // Populate the _bits table by setting all the bits corresponding to
            // the valid field values.
            //

            for (i = start - minValue; i <= (end - minValue); i += interval)
                _bits[i] = true;

            //
            // Make sure we remember the minimum value set so far Keep track of
            // the highest and lowest values that have been added to this field
            // so far.
            //

            if (_minValueSet > start)
                _minValueSet = start;

            i += (minValue - interval);

            if (_maxValueSet < i)
                _maxValueSet = i;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Alltrade.Integrador.Common.Schedule.Schedule.CronTab.CronTabField"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Alltrade.Integrador.Common.Schedule.Schedule.CronTab.CronTabField"/>.</returns>
        public override string ToString()
        {
            return ToString(null);
        }

        /// <summary>
        /// Tos the string.
        /// </summary>
        /// <returns>The string.</returns>
        /// <param name="format">Format.</param>
        public string ToString(string format)
        {
            var writer = new StringWriter(CultureInfo.InvariantCulture);

            switch (format)
            {
                case "G":
                case null:
                    Format(writer, true);
                    break;
                case "N":
                    Format(writer);
                    break;
                default:
                    throw new FormatException();
            }

            return writer.ToString();
        }

        /// <summary>
        /// Format the specified writer.
        /// </summary>
        /// <param name="writer">Writer.</param>
        public void Format(TextWriter writer)
        {
            Format(writer, false);
        }

        /// <summary>
        /// Format the specified writer and noNames.
        /// </summary>
        /// <param name="writer">Writer.</param>
        /// <param name="noNames">If set to <c>true</c> no names.</param>
        public void Format(TextWriter writer, bool noNames)
        {
            _impl.Format(this, writer, noNames);
        }
    }
}