using System;
using System.Threading.Tasks;
using Alltrade.Integrador.Common.Logger.Logger;
using Alltrade.Integrador.Common.Schedule.Schedule.Scheduling;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Alltrade.Integrador.Common.Schedule.Schedule
{
    /// <summary>
    /// Extension.
    /// </summary>
    public static class Extension
    {
        /// <summary>
        /// Adds the schedule task.
        /// </summary>
        /// <returns>The schedule task.</returns>
        /// <param name="services">Services.</param>
        /// <typeparam name="TScheduleTask">The 1st type parameter.</typeparam>
        public static IServiceCollection AddScheduleTask<TScheduleTask>(this IServiceCollection services)
            where TScheduleTask : class, IScheduledTask
        {
            return services.AddSingleton<IScheduledTask, TScheduleTask>();
        }

        /// <summary>
        /// Adds the scheduler.
        /// </summary>
        /// <returns>The scheduler.</returns>
        /// <param name="services">Services.</param>
        public static IServiceCollection AddScheduler(this IServiceCollection services)
        {
            return services.AddSingleton<IHostedService, SchedulerHostedService>();
        }

        /// <summary>
        /// Adds the scheduler.
        /// </summary>
        /// <returns>The scheduler.</returns>
        /// <param name="services">Services.</param>
        /// <param name="unobservedTaskExceptionHandler">Unobserved task exception handler.</param>
        public static IServiceCollection AddScheduler(this IServiceCollection services, EventHandler<UnobservedTaskExceptionEventArgs> unobservedTaskExceptionHandler)
        {
            return services.AddSingleton<IHostedService, SchedulerHostedService>(serviceProvider =>
            {
                var instance = new SchedulerHostedService(serviceProvider.GetService<IBaseLogger>(), serviceProvider.GetServices<IScheduledTask>());
                instance.UnobservedTaskException += unobservedTaskExceptionHandler;
                
                return instance;
            });
        }
    }
}