﻿using Alltrade.Integrador.Common.Commands;
using Alltrade.Integrador.Common.Commands.Integrador;
using Alltrade.Integrador.Common.Commands.Integrador.Agent;
using Alltrade.Integrador.Common.Controllers;
using Alltrade.Integrador.Common.Logger.Logger;
using Alltrade.Integrador.Common.RabbitMq;
using Alltrade.Integrador.Microservices.Integrador.Agent.Api.Handlers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Alltrade.Integrador.Microservices.Integrador.Agent.Api
{
    /// <summary>
    /// Startup.
    /// </summary>

    public class Startup
    {
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>The configuration.</value>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Microservices.Integrador.Agent.Api.Startup"/> class.
        /// </summary>
        /// <param name="configuration">Configuration.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Configures the services.
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddCommonControllers<Startup>();
            services.AddLogger();
            services.AddRabbitMq(Configuration);
            services.AddScoped<ICommandHandler<AgentCreateCommand>, AgentCreateCommandHandler>();
        }

        /// <summary>
        /// Configure the specified app and env.
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">App.</param>
        /// <param name="env">Env.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
