﻿using Alltrade.Integrador.Common.Commands;
using Alltrade.Integrador.Common.Commands.Integrador;
using Microsoft.Extensions.Logging;
using RawRabbit;
using System;
using System.Threading.Tasks;
using Alltrade.Integrador.Common.Commands.Integrador.Agent;
using Alltrade.Integrador.Common.Events.Integrador.Agent;

namespace Alltrade.Integrador.Microservices.Integrador.Agent.Api.Handlers
{
    /// <summary>
    /// Create agent handler.
    /// </summary>
    public class AgentCreateCommandHandler : ICommandHandler<AgentCreateCommand>
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// The bus client.
        /// </summary>
        private readonly IBusClient _busClient;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Microservices.Integrador.Agent.Api.Handlers.CreateAgentHandler"/> class.
        /// </summary>
        /// <param name="busClient">Bus client.</param>
        /// <param name="logger">Logger.</param>
        public AgentCreateCommandHandler(IBusClient busClient, ILogger<AgentCreateCommandHandler> logger)
        {
            _busClient = busClient;
            _logger = logger;
        }

        /// <summary>
        /// Handles the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="command">Command.</param>
        public async Task HandleAsync(AgentCreateCommand command)
        {
            try
            {
                _logger.LogInformation($"Creating agent: '{command.Agent.AgentId}' for environment: '{command.EnvironmentId}'.");
                _logger.LogInformation($"Agent: '{command.Agent.AlternativeIdentifier}' was created for environment: '{command.EnvironmentId}'.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                await _busClient.PublishAsync(new AgentCreateEventRejected()
                {
                    CodeId = command.Agent.AlternativeIdentifier,
                    CodeDescription = ex.Message,
                    Reason = ""
                });
            }
        }
    }
}
