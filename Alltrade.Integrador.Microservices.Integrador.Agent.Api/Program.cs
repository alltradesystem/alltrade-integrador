﻿using Alltrade.Integrador.Common.Commands.Integrador.Agent;
using Alltrade.Integrador.Common.ServiceHost.Services;

namespace Alltrade.Integrador.Microservices.Integrador.Agent.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ServiceHost.Create<Startup>(args)
                .UseRabbitMq()
                .SubscribeToCommand<AgentCreateCommand>()
                .Build()
                .Run();
        }
    }
}
