﻿using Alltrade.Integrador.Common.Contexts.EntityContexts.Integrador;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Alltrade.Integrador.Common.Contexts.Extension
{
    /// <summary>
    /// Extensions.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Adds the sql server.
        /// </summary>
        /// <param name="services">Services.</param>
        /// <param name="configuration">Configuration.</param>
        /// <param name="configurationSectionName">Configuration section name.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static void AddSqlServer<T>(this IServiceCollection services, IConfiguration configuration,
                                           string configurationSectionName)
            where T : BaseContext
        {
            services.AddDbContext<T>(options =>
                                     options.UseSqlServer(configuration.GetConnectionString(configurationSectionName)));
        }
    }
}
