﻿using Microsoft.EntityFrameworkCore;

namespace Alltrade.Integrador.Common.Contexts.EntityContexts.Integrador
{
    /// <summary>
    /// Base context.
    /// </summary>
    public class BaseContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Contexts.EntityContexts.Integrador.BaseContext"/> class.
        /// </summary>
        public BaseContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Contexts.EntityContexts.Integrador.BaseContext"/> class.
        /// </summary>
        /// <param name="options">Options.</param>
        public BaseContext(DbContextOptions<BaseContext> options) 
            : base(options)
        {
        }
    }
}
