﻿using System;
using Alltrade.Integrador.Common.Models.Entity.Integrador;
using Microsoft.EntityFrameworkCore;

namespace Alltrade.Integrador.Common.Contexts.EntityContexts.Integrador
{
    /// <summary>
    /// Integrador context.
    /// </summary>
	public class IntegradorContext : BaseContext
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Contexts.EntityContexts.Integrador.IntegradorContext"/> class.
        /// </summary>
        public IntegradorContext()
        {
        }

        /// <summary>
        /// Ons the model creating.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            modelBuilder.Entity<Agent>(entity => 
            {

            });
        }
    }
}
