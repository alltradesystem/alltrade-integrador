﻿using Alltrade.Integrador.Common.Schedule.Schedule.Scheduling;
using Alltrade.Integrador.Common.Schedule.Schedule.Scheduling.Attributes;
using RawRabbit;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Alltrade.Integrador.Common.Commands.Integrador.Agent;
using Alltrade.Integrador.Common.Models.Entity.Integrador;
using Cassandra;
using Alltrade.Integrador.Common.Logger.Logger;
using System.Net;

namespace Alltrade.Integrador.Microservices.Customers.AlltradeTest.Schedules
{
    [ScheduleTask(Name = "Agent Schedule", Schedule = "* * * * *")]
    public class AgentSchedule : IScheduledTask
    {
        private readonly IBaseLogger _logger;

        private readonly IBusClient _busClient;

        private readonly ISession _session;

        public AgentSchedule(IBaseLogger logger, IBusClient busClient, ISession session)
        {
            _logger = logger;
            _busClient = busClient;
            _session = session;
        }

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.Log("Running Task...", LogEvent.Debug);

            var batch = new BatchStatement();
            var ps = _session.Prepare("insert into loads (machine, cpu, mtime, load) values(?, ?, ?, ?)");

            for (int i = 1; i <= 1_000_000; i++)
            {
                batch.Add(ps.Bind(IPAddress.Any, i, TimeUuid.NewId(), (float)i));
                if (i % 1000 == 0) 
                {
                    _session.Execute(batch);
                    batch = new BatchStatement();
                }
            }

            //var rs = _session.Execute("SELECT * FROM loads");
            //foreach (var row in rs)
            //{

            //    var cpu = row.GetValue<int>("cpu");
            //    var load = row.GetValue<float>("load");

            //    _logger.Log($"CPU: {cpu}, LOAD: {load}", LogEvent.Debug);
            //}

            //var agent = new AgentCreateCommand();
            //agent.EnvironmentId = Guid.NewGuid();
            //agent.Agent = new Agent()
            //{
            //    AlternativeIdentifier = "Teste"
            //};           

            //await _busClient.PublishAsync(agent, token: cancellationToken);
            //await Task.Delay(5000, cancellationToken);
        }
    }
}
