﻿using Alltrade.Integrador.Common.ServiceHost.Services;

namespace Alltrade.Integrador.Microservices.Customers.AlltradeTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ServiceHost.Create<Startup>(args)
                .UseRabbitMq()
                .Build()
                .Run();
        }
    }
}
