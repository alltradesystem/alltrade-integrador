﻿using Alltrade.Integrador.Common.Auth.Auth;
using Alltrade.Integrador.Common.Cassandra.Cassandra;
using Alltrade.Integrador.Common.Controllers;
using Alltrade.Integrador.Common.Logger.Logger;
using Alltrade.Integrador.Common.RabbitMq;
using Alltrade.Integrador.Common.Schedule.Schedule;
using Alltrade.Integrador.Microservices.Customers.AlltradeTest.Schedules;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace Alltrade.Integrador.Microservices.Customers.AlltradeTest
{

    /// <summary>
    /// Startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>The configuration.</value>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Microservices.Customers.AlltradeTest.Startup"/> class.
        /// </summary>
        /// <param name="configuration">Configuration.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogger();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddCommonControllers<Startup>();
            services.AddCassandra(Configuration);
            services.AddRabbitMq(Configuration);
            services.AddJwt(Configuration);

            services.AddScheduleTask<AgentSchedule>();
            services.AddScheduler((sender, args) =>
            {
                Debug.WriteLine(args.Exception.Message);
                args.SetObserved();
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">App.</param>
        /// <param name="env">Env.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
