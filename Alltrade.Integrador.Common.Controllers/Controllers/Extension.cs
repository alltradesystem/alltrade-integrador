﻿using Alltrade.Integrador.Common.Controllers.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace Alltrade.Integrador.Common.Controllers
{
    /// <summary>
    /// Extension.
    /// </summary>
    public static class Extension
    {
        /// <summary>
        /// Adds the common controllers.
        /// </summary>
        /// <param name="services">Services.</param>
        /// <typeparam name="TStartup">The 1st type parameter.</typeparam>
        public static void AddCommonControllers<TStartup>(this IServiceCollection services)
            where TStartup : class
        {
            StartupType.Instance.Type = typeof(TStartup);
            services.AddSingleton<IStartupType>(_ => StartupType.Instance);

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddApplicationPart(StartupType.Instance.Type.Assembly)
                .AddControllersAsServices();
        }
    }
}
