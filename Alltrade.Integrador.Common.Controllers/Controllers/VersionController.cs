﻿using Alltrade.Integrador.Common.Controllers.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Alltrade.Integrador.Common.Controllers.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Version controller.
    /// </summary>
    [Route("api/[controller]")]
    public class VersionController : Controller
    {
        /// <summary>
        /// The type of the startup.
        /// </summary>
        private readonly IStartupType _startupType;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Controllers.Controllers.VersionController"/> class.
        /// </summary>
        /// <param name="iStartupType">I startup type.</param>
        public VersionController(IStartupType iStartupType)
        {
            _startupType = iStartupType;
        }

        /// <summary>
        /// Get this instance.
        /// </summary>
        /// <returns>The get.</returns>
        [HttpGet]
        public IActionResult Get()
        {
            var assembly = _startupType.Type.Assembly;
            return Content($"Microservice: {assembly.GetName().Name}, Version: {assembly.GetName().Version}");
        }
    }
}
