﻿using System;

namespace Alltrade.Integrador.Common.Controllers.Infrastructure
{
    /// <summary>
    /// Startup type.
    /// </summary>
    internal class StartupType : IStartupType
    {
        /// <summary>
        /// The instance holder.
        /// </summary>
        private static readonly Lazy<StartupType> instanceHolder = new Lazy<StartupType>(() => new StartupType());

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static StartupType Instance => instanceHolder.Value;

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public Type Type { get; set; }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Controllers.Infrastructure.StartupType"/> class.
        /// </summary>
        private StartupType()
        {
        }
    }
}
