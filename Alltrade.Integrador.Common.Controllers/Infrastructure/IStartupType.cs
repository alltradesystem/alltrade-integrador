﻿using System;

namespace Alltrade.Integrador.Common.Controllers.Infrastructure
{
    /// <summary>
    /// Startup type.
    /// </summary>
    public interface IStartupType
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        Type Type { get; set; }
    }
}
