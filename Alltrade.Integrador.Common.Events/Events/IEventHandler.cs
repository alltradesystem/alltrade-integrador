﻿using System.Threading.Tasks;

namespace Alltrade.Integrador.Common.Events
{
    /// <summary>
    /// Event handler.
    /// </summary>
    public interface IEventHandler<in TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// Handles the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="event">Event.</param>
        Task HandleAsync(TEvent @event);
    }
}
