﻿using System;

namespace Alltrade.Integrador.Common.Events
{
    /// <summary>
    /// Authenticated event.
    /// </summary>
    public interface IAuthenticatedEvent : IEvent
    {
        /// <summary>
        /// Gets the environment identifier.
        /// </summary>
        /// <value>The environment identifier.</value>
        Guid EnvironmentId { get; }
    }
}
