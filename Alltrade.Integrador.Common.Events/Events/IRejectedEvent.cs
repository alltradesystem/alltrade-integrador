﻿namespace Alltrade.Integrador.Common.Events
{
    /// <summary>
    /// Rejected event.
    /// </summary>
    public interface IRejectedEvent : IEvent
    {
        /// <summary>
        /// Gets or sets the code identifier.
        /// </summary>
        /// <value>The code identifier.</value>
        string CodeId { get; set; }

        /// <summary>
        /// Gets or sets the code description.
        /// </summary>
        /// <value>The code description.</value>
        string CodeDescription { get; set; }

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        /// <value>The reason.</value>
        string Reason { get; set; }
    }
}
