﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Alltrade.Integrador.Common.Events.Integrador.Agent
{
    /// <summary>
    /// Agent create event rejected.
    /// </summary>
    public class AgentCreateEventRejected : IRejectedEvent
    {
        /// <summary>
        /// Gets or sets the code identifier.
        /// </summary>
        /// <value>The code identifier.</value>
        public string CodeId { get; set; }

        /// <summary>
        /// Gets or sets the code description.
        /// </summary>
        /// <value>The code description.</value>
        public string CodeDescription { get; set; }

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        /// <value>The reason.</value>
        public string Reason { get; set; }
    }
}
