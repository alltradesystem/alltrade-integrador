﻿using System;
using Alltrade.Integrador.Common.Models.Entity.Integrador;

namespace Alltrade.Integrador.Common.Events.Integrador
{
    /// <summary>
    /// Event agent created.
    /// </summary>
    public class AgentCreatedEvent : IEvent
    {
        /// <summary>
        /// Gets the environment identifier.
        /// </summary>
        /// <value>The environment identifier.</value>
        public Guid EnvironmentId { get; }

        ///// <summary>
        ///// Gets or sets the agent.
        ///// </summary>
        ///// <value>The agent.</value>
        //public Agent Agent { get; set; }

        ///// <summary>
        ///// Initializes a new instance of the
        ///// <see cref="T:Alltrade.Integrador.Common.Events.EventsIntegrador.EventAgentCreated"/> class.
        ///// </summary>
        //protected AgentCreatedEvent()
        //{
        //}

        ///// <summary>
        ///// Initializes a new instance of the
        ///// <see cref="T:Alltrade.Integrador.Common.Events.EventsIntegrador.EventAgentCreated"/> class.
        ///// </summary>
        ///// <param name="agent">Agent.</param>
        //public AgentCreatedEvent(Agent agent)
        //{
        //    EnvironmentId = agent.Environment.EnvironmentId;
        //    Agent = agent;
        //}
    }
}
