﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Alltrade.Integrador.Common.Models.Entity.Integrador
{
    /// <summary>
    /// Environment.
    /// </summary>
    [Table("Environment", Schema = "integrador")]
    public class Environment
    {
        /// <summary>
        /// Gets or sets the environment identifier.
        /// </summary>
        /// <value>The environment identifier.</value>
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid EnvironmentId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [Column(Order = 1)]
        public string Name { get; set; }
    }
}
