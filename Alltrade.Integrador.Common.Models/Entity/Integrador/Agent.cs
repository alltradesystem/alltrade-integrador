﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Alltrade.Integrador.Common.Models.Entity.Integrador
{
    /// <summary>
    /// Agent.
    /// </summary>
    [Table("Agent", Schema = "integrador")]
    public class Agent
    {
        /// <summary>
        /// Gets or sets the agent identifier.
        /// </summary>
        /// <value>The agent identifier.</value>
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid AgentId { get; set; }

        /// <summary>
        /// Gets or sets the environment.
        /// </summary>
        /// <value>The environment.</value>
        public Environment Environment { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the active.
        /// </summary>
        /// <value>The active.</value>
        public bool? Active { get; set; }

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>The image.</value>
        public string Image { get; set; }

        /// <summary>
        /// Gets or sets the alternative identifier.
        /// </summary>
        /// <value>The alternative identifier.</value>
        public string AlternativeIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the type of the agent.
        /// </summary>
        /// <value>The type of the agent.</value>
        public int? AgentType { get; set; }

        /// <summary>
        /// Gets or sets the login.
        /// </summary>
        /// <value>The login.</value>
        public string Login { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the center web user.
        /// </summary>
        /// <value>The center web user.</value>
        public bool? CenterWebUser { get; set; }

        /// <summary>
        /// Gets or sets the mobile user.
        /// </summary>
        /// <value>The mobile user.</value>
        public bool? MobileUser { get; set; }

        /// <summary>
        /// Gets or sets the center web user role.
        /// </summary>
        /// <value>The center web user role.</value>
        public char CenterWebUserRole { get; set; }

        /// <summary>
        /// Gets or sets the access role.
        /// </summary>
        /// <value>The access role.</value>
        public string AccessRole { get; set; }

        /// <summary>
        /// Gets or sets the input web as another user.
        /// </summary>
        /// <value>The input web as another user.</value>
        public bool? InputWebAsAnotherUser { get; set; }

        /// <summary>
        /// Gets or sets the observation.
        /// </summary>
        /// <value>The observation.</value>
        public string Observation { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city.</value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the neighborhood.
        /// </summary>
        /// <value>The neighborhood.</value>
        public string Neighborhood { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        /// <value>The street.</value>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the type of the street.
        /// </summary>
        /// <value>The type of the street.</value>
        public string StreetType { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        /// <value>The zip code.</value>
        public int? ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the street number.
        /// </summary>
        /// <value>The street number.</value>
        public int? StreetNumber { get; set; }

        /// <summary>
        /// Gets or sets the street complement.
        /// </summary>
        /// <value>The street complement.</value>
        public string StreetComplement { get; set; }

        /// <summary>
        /// Gets or sets the cell phone idd.
        /// </summary>
        /// <value>The cell phone idd.</value>
        public int? CellPhoneIdd { get; set; }

        /// <summary>
        /// Gets or sets the cell phone std.
        /// </summary>
        /// <value>The cell phone std.</value>
        public int? CellPhoneStd { get; set; }

        /// <summary>
        /// Gets or sets the cell phone.
        /// </summary>
        /// <value>The cell phone.</value>
        public int? CellPhone { get; set; }

        /// <summary>
        /// Gets or sets the phone idd.
        /// </summary>
        /// <value>The phone idd.</value>
        public int? PhoneIdd { get; set; }

        /// <summary>
        /// Gets or sets the phone std.
        /// </summary>
        /// <value>The phone std.</value>
        public int? PhoneStd { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>The phone.</value>
        public int? Phone { get; set; }

        /// <summary>
        /// Gets or sets the validate client.
        /// </summary>
        /// <value>The validate client.</value>
        public char? ValidateClient { get; set; }

        /// <summary>
        /// Gets or sets the change password.
        /// </summary>
        /// <value>The change password.</value>
        public bool? ChangePassword { get; set; }

        /// <summary>
        /// Gets or sets the last synchronism date.
        /// </summary>
        /// <value>The last synchronism date.</value>
        public DateTime LastSynchronismDate { get; set; }

        /// <summary>
        /// Gets or sets the last synchronism time.
        /// </summary>
        /// <value>The last synchronism time.</value>
        public DateTime LastSynchronismTime { get; set; }

        /// <summary>
        /// Gets or sets the lock login in change imei.
        /// </summary>
        /// <value>The lock login in change imei.</value>
        public bool? LockLoginInChangeImei { get; set; }

        /// <summary>
        /// Gets or sets the imei last synchronism.
        /// </summary>
        /// <value>The imei last synchronism.</value>
        public string ImeiLastSynchronism { get; set; }

        /// <summary>
        /// Gets or sets the memorize password mobile.
        /// </summary>
        /// <value>The memorize password mobile.</value>
        public bool? MemorizePasswordMobile { get; set; }

        /// <summary>
        /// Gets or sets the geo location.
        /// </summary>
        /// <value>The geo location.</value>
        public string GeoLocation { get; set; }

        /// <summary>
        /// Gets or sets the custom fields.
        /// </summary>
        /// <value>The custom fields.</value>
        [NotMapped]
        public ICollection<CustomField> CustomFields { get; set; }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Models.Entity.Integrador.Agent"/> class.
        /// </summary>
        public Agent()
        {
            CustomFields = new HashSet<CustomField>();
        }
    }
}
