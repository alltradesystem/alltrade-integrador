﻿using System;

namespace Alltrade.Integrador.Common.Exceptions.Exceptions.Base
{
    /// <summary>
    /// Alltrade integrador exception.
    /// </summary>
    public class AlltradeIntegradorException : Exception
    {
        /// <summary>
        /// Gets the code identifier.
        /// </summary>
        /// <value>The code identifier.</value>
        public string CodeId { get; }

        /// <summary>
        /// Gets the code description.
        /// </summary>
        /// <value>The code description.</value>
        public string CodeDescription { get; }

        /// <summary>
        /// Gets the reason.
        /// </summary>
        /// <value>The reason.</value>
        public string Reason { get; }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Exceptions.Exceptions.AlltradeIntegradorException"/> class.
        /// </summary>
        public AlltradeIntegradorException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Exceptions.Exceptions.AlltradeIntegradorException"/> class.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="args">Arguments.</param>
        public AlltradeIntegradorException(string message, params object[] args) : this(string.Empty, string.Empty, string.Empty, message, args)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Exceptions.Exceptions.AlltradeIntegradorException"/> class.
        /// </summary>
        /// <param name="codeId">Code identifier.</param>
        /// <param name="codeDescription">Code description.</param>
        /// <param name="reason">Reason.</param>
        /// <param name="message">Message.</param>
        /// <param name="args">Arguments.</param>
        public AlltradeIntegradorException(string codeId, string codeDescription, string reason, string message, params object[] args) : this(null, codeId, codeDescription, reason, message, args)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Exceptions.Exceptions.AlltradeIntegradorException"/> class.
        /// </summary>
        /// <param name="innerException">Inner exception.</param>
        /// <param name="message">Message.</param>
        /// <param name="args">Arguments.</param>
        public AlltradeIntegradorException(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, string.Empty, string.Empty, message, args)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Alltrade.Integrador.Common.Exceptions.Exceptions.AlltradeIntegradorException"/> class.
        /// </summary>
        /// <param name="innerException">Inner exception.</param>
        /// <param name="codeId">Code identifier.</param>
        /// <param name="codeDescription">Code description.</param>
        /// <param name="reason">Reason.</param>
        /// <param name="message">Message.</param>
        /// <param name="args">Arguments.</param>
        public AlltradeIntegradorException(Exception innerException, string codeId, string codeDescription, string reason, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            CodeId = codeId;
            CodeDescription = codeDescription;
            Reason = reason;
        }
    }
}
