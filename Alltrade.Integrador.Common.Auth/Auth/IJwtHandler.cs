﻿using System;

namespace Alltrade.Integrador.Common.Auth.Auth
{
    /// <summary>
    /// Jwt handler.
    /// </summary>
    public interface IJwtHandler
    {
        /// <summary>
        /// Create the specified environmentId and userId.
        /// </summary>
        /// <returns>The create.</returns>
        /// <param name="environmentId">Environment identifier.</param>
        /// <param name="userId">User identifier.</param>
        JsonWebToken Create(Guid environmentId, Guid userId);
    }
}
