﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace Alltrade.Integrador.Common.Auth.Auth
{
    /// <summary>
    /// Jwt handler.
    /// </summary>
    public class JwtHandler : IJwtHandler
    {
        /// <summary>
        /// The jwt security token handler.
        /// </summary>
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler = new JwtSecurityTokenHandler();

        /// <summary>
        /// The options.
        /// </summary>
        private readonly JwtOptions _options;

        /// <summary>
        /// The issuer signing key.
        /// </summary>
        private readonly SecurityKey _issuerSigningKey;

        /// <summary>
        /// The signing credentials.
        /// </summary>
        private readonly SigningCredentials _signingCredentials;

        /// <summary>
        /// The jwt header.
        /// </summary>
        private readonly JwtHeader _jwtHeader;

        /// <summary>
        /// The token validation parameters.
        /// </summary>
        private readonly TokenValidationParameters _tokenValidationParameters;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Alltrade.Integrador.Common.Auth.Auth.JwtHandler"/> class.
        /// </summary>
        /// <param name="options">Options.</param>
        public JwtHandler(IOptions<JwtOptions> options)
        {
            _options = options.Value;
            _issuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.SecretKey));
            _signingCredentials = new SigningCredentials(_issuerSigningKey, SecurityAlgorithms.HmacSha256);
            _jwtHeader = new JwtHeader(_signingCredentials);

            _tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidIssuer = _options.Issuer,
                IssuerSigningKey = _issuerSigningKey
            };
        }

        /// <summary>
        /// Create the specified environmentId and userId.
        /// </summary>
        /// <returns>The create.</returns>
        /// <param name="environmentId">Environment identifier.</param>
        /// <param name="userId">User identifier.</param>
        public JsonWebToken Create(Guid environmentId, Guid userId)
        {
            var nowUtc = DateTime.UtcNow;
            var expires = nowUtc.AddMinutes(_options.ExpiryMinutes);
            var centuryBegin = new DateTime(1970, 1, 1).ToUniversalTime();
            var exp = (long)(new TimeSpan(expires.Ticks - centuryBegin.Ticks).TotalSeconds);
            var now = (long)(new TimeSpan(nowUtc.Ticks - centuryBegin.Ticks).TotalSeconds);

            var payload = new JwtPayload
            {
                {"sub", $"{environmentId}_{userId}"},
                {"iss", _options.Issuer},
                {"iat", now},
                {"exp", exp},
                {"environment_id", environmentId},
                {"user_id", userId}
            };
            var jwt = new JwtSecurityToken(_jwtHeader, payload);
            var token = _jwtSecurityTokenHandler.WriteToken(jwt);

            return new JsonWebToken
            {
                Token = token,
                Expires = exp
            };
        }
    }
}
