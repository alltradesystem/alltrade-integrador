﻿using System;

namespace Alltrade.Integrador.Common.Commands.Integrador.Agent
{
    /// <summary>
    /// Command agent create.
    /// </summary>
    public class AgentCreateCommand : ICommand
    {
        /// <summary>
        /// Gets or sets the environment identifier.
        /// </summary>
        /// <value>The environment identifier.</value>
        public Guid EnvironmentId { get; set; }

        /// <summary>
        /// Gets or sets the agent.
        /// </summary>
        /// <value>The agent.</value>
        public Models.Entity.Integrador.Agent Agent { get; set; }
    }
}
