﻿using System.Threading.Tasks;

namespace Alltrade.Integrador.Common.Commands
{
    /// <summary>
    /// Command handler.
    /// </summary>
    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        /// <summary>
        /// Handles the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="command">Command.</param>
        Task HandleAsync(TCommand command);
    }
}
