﻿using System;

namespace Alltrade.Integrador.Common.Commands
{
    /// <summary>
    /// Authenticated command.
    /// </summary>
    public interface IAuthenticatedCommand : ICommand
    {
        /// <summary>
        /// Gets or sets the environment identifier.
        /// </summary>
        /// <value>The environment identifier.</value>
        Guid EnvironmentId { get; set; }
    }
}
