﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;

namespace Alltrade.Integrador.Common.Logger.Logger
{
    /// <summary>
    /// Extensions.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Adds the logger.
        /// </summary>
        /// <returns>The logger.</returns>
        /// <param name="services">Services.</param>
        public static void AddLogger(this IServiceCollection services) 
        {
            services.AddSingleton<IBaseLogger>(_ => new BaseLogger());
        }

        /// <summary>
        /// Uses the logger.
        /// </summary>
        /// <returns>The logger.</returns>
        /// <param name="builder">Builder.</param>
        /// <param name="configuration">Configuration.</param>
        public static IWebHostBuilder UseLogger(this IWebHostBuilder builder, IConfiguration configuration) 
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom
                .Configuration(configuration)
                .CreateLogger();
                
            return builder.UseSerilog();
        }
    }
}
