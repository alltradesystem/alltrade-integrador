﻿using System;
using RestSharp;

namespace Alltrade.Integrador.Common.Processors.Processors
{
    public class ProcessorResult
    {
        public IRestResponse Response { get; set; }

        public bool IsSuccess { get; set; }

        public bool IsError { get; set; }

        public Exception Error { get; set; }

        public void MarkAsSuccess(IRestResponse response)
        {
            Response = response;
            IsSuccess = response.IsSuccessful;
            IsError = !response.IsSuccessful;
            Error = response.ErrorException;
        }

        public void MarkAsError(Exception ex = null)
        {
            Response = null;
            IsSuccess = false;
            IsError = true;
            Error = ex;
        }
    }
}
