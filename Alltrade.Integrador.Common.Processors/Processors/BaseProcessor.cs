﻿using RestSharp;
using System;
using System.Web;

namespace Alltrade.Integrador.Common.Processors.Processors
{
    public abstract class BaseProcessor
    {
        protected RestClient Client { get; set; } = null;

        protected RestRequest Request { get; set; } = null;

        protected string EndpointUrl { get; } = "";

        public abstract string ResourceUrl { get; }

        public abstract string TokenUrl { get; }

        public abstract Method HttpMethod { get; }

        public abstract DataFormat DataFormat { get; }

        public abstract string XmlContent { get; }

        protected BaseProcessor()
        {
            Client = new RestClient($"{EndpointUrl}/{TokenUrl}");
            Request = new RestRequest(ResourceUrl, HttpMethod, DataFormat);
        }

        public virtual ProcessorResult Process(Parameters parameters)
        {
            var processorResult = new ProcessorResult();
            try
            {
                Request.AddBody($"data={HttpUtility.UrlEncode(XmlContent)}");
                Client.ExecuteAsync(Request, processorResult.MarkAsSuccess);
            }
            catch (Exception ex)
            {
                processorResult.MarkAsError(ex);
            }
            return processorResult;
        }
    }
}
