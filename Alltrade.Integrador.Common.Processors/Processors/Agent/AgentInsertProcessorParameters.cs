﻿using System.Text;
using RestSharp;

namespace Alltrade.Integrador.Common.Processors.Processors.Agent
{
    public class AgentInsertProcessorParameters
    {
        public Models.Entity.Integrador.Agent Agent { get; set; }
    }
}
