﻿using System.Linq;
using System.Text;
using RestSharp;

namespace Alltrade.Integrador.Common.Processors.Processors.Agent
{
    public class AgentInsertProcessor : BaseProcessor
    {
        public override string ResourceUrl => "agent.xml";

        public override string TokenUrl => "";

        public override Method HttpMethod => Method.POST;

        public override DataFormat DataFormat => DataFormat.Xml;

        public override string XmlContent
        {
            get
            {
                var agent = _parameters.Agent;
                var sb = new StringBuilder(string.Empty);

                sb.Append("<agent>");
                sb.AppendFormat("<active>{0}</active>", agent.Active.ToString().ToLower());

                if (agent.AgentType.HasValue)
                    sb.AppendFormat("<agentType><alternativeIdentifier>{0}</alternativeIdentifier></agentType>", agent.AgentType.Value);

                sb.AppendFormat("<login>{0}</login>", agent.Login.Trim());
                sb.AppendFormat("<name><![CDATA[{0}]]></name>", agent.Name.Trim());
                sb.AppendFormat("<password>{0}</password>", agent.Password.Trim());
                sb.AppendFormat("<email><![CDATA[{0}]]></email>", agent.Email.Trim());
                sb.AppendFormat("<alternativeIdentifier>{0}</alternativeIdentifier>", agent.AlternativeIdentifier.Trim());
                sb.AppendFormat("<changePassword>{0}</changePassword>", agent.ChangePassword.ToString() ?? "false");
                sb.AppendFormat("<memorizePasswordMobile>{0}</memorizePasswordMobile>", agent.MemorizePasswordMobile.ToString() ?? "false");

                sb.Append("<biUser>false</biUser>");
                sb.Append("<mobileUser>true</mobileUser>");
                sb.Append("<centerwebUserRole>O</centerwebUserRole>");

                sb.AppendFormat("<city><![CDATA[{0}]]></city>", agent.City);
                sb.AppendFormat("<neighborhood><![CDATA[{0}]]></neighborhood>", agent.Neighborhood);
                sb.AppendFormat("<country><![CDATA[{0}]]></country>", agent.Country);
                sb.AppendFormat("<state><![CDATA[{0}]]></state>", agent.State);
                sb.AppendFormat("<street><![CDATA[{0}]]></street>", agent.Street);
                sb.AppendFormat("<streetType><![CDATA[{0}]]></streetType>", agent.StreetType);
                sb.AppendFormat("<zipCode>{0}</zipCode>", agent.ZipCode);
                sb.AppendFormat("<streetNumber>{0}</streetNumber>", agent.StreetNumber);
                sb.AppendFormat("<streetComplement><![CDATA[{0}]]></streetComplement>", agent.StreetComplement);

                if (!string.IsNullOrEmpty(agent.Image))
                {
                    sb.Append("<image>");
                    sb.AppendFormat("<imageUrlImport><![CDATA[{0}]]></imageUrlImport>", agent.Image);
                    sb.Append("</image>");
                }
                
                if (agent.CustomFields.Any())
                {
                    sb.Append("<customFields>");
                    agent.CustomFields.ToList().ForEach(customField =>
                    {
                        sb.AppendFormat("<{0}>", "Nome do campo");
                        sb.AppendFormat("<![CDATA[{0}]]>", "Valor Interno");
                        sb.AppendFormat("</{0}>", "/Nome do Campo");
                    });
                    sb.Append("</customFields>");
                }

                sb.Append("</agent>");

                return sb.ToString();
            }
        }

        private readonly AgentInsertProcessorParameters _parameters;

        public AgentInsertProcessor(AgentInsertProcessorParameters parameters)
        {
            _parameters = parameters;
        }
    }
}
