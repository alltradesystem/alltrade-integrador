﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Alltrade.Integrador.Common.Logger.Logger;

namespace Alltrade.Integrador.Common.Processors.AsyncDataAdapter
{
    public abstract class BaseAsyncDataAdapter : IBaseAsyncDataAdapter
    {
        protected readonly IBaseLogger BaseLogger;

        public bool GetAllData { get; set; }

        protected BaseAsyncDataAdapter(IBaseLogger baseLogger)
        {
            BaseLogger = baseLogger;
        }

        public void GetData(Parameters parameters)
        {
            throw new NotImplementedException();
        }

        public Task SendData(Parameters parameters)
        {
            throw new NotImplementedException();
        }
    }
}
