﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Alltrade.Integrador.Common.Processors.AsyncDataAdapter
{
    public interface IBaseAsyncDataAdapter
    {
        Task SendData(Parameters parameters);

        void GetData(Parameters parameters);
    }
}
