﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Alltrade.Integrador.Common.Logger.Logger;

namespace Alltrade.Integrador.Common.Processors.AsyncDataAdapter
{
    public class AgentAsyncDataAdapter : BaseAsyncDataAdapter
    {
        public AgentAsyncDataAdapter(IBaseLogger baseLogger) : base(baseLogger)
        {
        }

        public async Task SendData(Parameters parameters)
        {
        }

        public void GetData(Parameters parameters)
        {
        }
    }
}
